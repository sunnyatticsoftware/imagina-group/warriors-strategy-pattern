# warriors-strategy-pattern

¿Cómo podría conseguir que algo se pueda comportar de diferente forma?

Sin tocar el código de la función Iterate, haz que cada uno de los guerreros ataque de una forma diferente.
El ninja golpea con nunchaku, el boxeador da puñetazos, el zombie muerde, el karateka da patadas.